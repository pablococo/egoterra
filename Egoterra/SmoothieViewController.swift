//
//  SmoothieViewController.swift
//  Egoterra
//
//  Created by Pablo coco on 2016. 3. 25..
//  Copyright © 2016년 Egoterra. All rights reserved.
//

import UIKit

class SmoothieViewController : UITableViewController {

    var SmoothieList = Array<SmoothieVO>()
    
    override func viewDidLoad(){
        let svo = SmoothieVO()
        svo.thumbnail = "1_1.png"
        self.SmoothieList.append(svo)
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.SmoothieList.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let row = self.SmoothieList[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ListCell") as! SmoothieCell
        
        cell.thumbnail.image = UIImage(named: row.thumbnail!)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        NSLog("Touch Table Row at %d", indexPath.row)
    }
}
