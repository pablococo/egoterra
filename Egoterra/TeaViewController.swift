//
//  TeaViewController.swift
//  Egoterra
//
//  Created by Pablo coco on 2016. 3. 25..
//  Copyright © 2016년 Egoterra. All rights reserved.
//

import UIKit

class TeaViewController : UITableViewController{
    var TeaList = Array<TeaVO>()
    
    
    override func viewDidLoad() {
        var tvo = TeaVO()
        tvo.thumbnail = "2-1.png"
        
        self.TeaList.append(tvo)
        
        tvo = TeaVO()
        tvo.thumbnail = "2-2.png"
        self.TeaList.append(tvo)
        
        tvo = TeaVO()
        tvo.thumbnail = "2-3.png"
        self.TeaList.append(tvo)
        
        tvo = TeaVO()
        tvo.thumbnail = "2-4.png"
        self.TeaList.append(tvo)
        
        tvo = TeaVO()
        tvo.thumbnail = "2-5.png"
        self.TeaList.append(tvo)
        
        tvo = TeaVO()
        tvo.thumbnail = "2-6.png"
        self.TeaList.append(tvo)
        
        tvo = TeaVO()
        tvo.thumbnail = "2-7.png"
        self.TeaList.append(tvo)
        
        tvo = TeaVO()
        tvo.thumbnail = "2-8.png"
        self.TeaList.append(tvo)
        
        tvo = TeaVO()
        tvo.thumbnail = "2-9.png"
        self.TeaList.append(tvo)
        
        tvo = TeaVO()
        tvo.thumbnail = "2-10.png"
        self.TeaList.append(tvo)
        
        tvo = TeaVO()
        tvo.thumbnail = "2-11.png"
        self.TeaList.append(tvo)
        
        tvo = TeaVO()
        tvo.thumbnail = "2-12.png"
        self.TeaList.append(tvo)
        
        tvo = TeaVO()
        tvo.thumbnail = "2-13.png"
        self.TeaList.append(tvo)
        
        tvo = TeaVO()
        tvo.thumbnail = "2-14.png"
        self.TeaList.append(tvo)
        
        tvo = TeaVO()
        tvo.thumbnail = "2-15.png"
        self.TeaList.append(tvo)
        
        tvo = TeaVO()
        tvo.thumbnail = "2-16.png"
        self.TeaList.append(tvo)
        
        tvo = TeaVO()
        tvo.thumbnail = "2-17.png"
        self.TeaList.append(tvo)
        
        tvo = TeaVO()
        tvo.thumbnail = "2-18.png"
        self.TeaList.append(tvo)
        
        tvo = TeaVO()
        tvo.thumbnail = "2-19.png"
        self.TeaList.append(tvo)
        
        tvo = TeaVO()
        tvo.thumbnail = "2-20.png"
        self.TeaList.append(tvo)
        
        tvo = TeaVO()
        tvo.thumbnail = "2-21.png"
        self.TeaList.append(tvo)
        
        tvo = TeaVO()
        tvo.thumbnail = "2-22.png"
        self.TeaList.append(tvo)
        
        tvo = TeaVO()
        tvo.thumbnail = "2-23.png"
        self.TeaList.append(tvo)
        
        
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //
        return self.TeaList.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let row = self.TeaList[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ListCell") as! TeaCell
        
        
        cell.thumbnail.image = UIImage(named: row.thumbnail!)
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //
        NSLog("Touch Table Row at %d", indexPath.row)
    }
    @IBAction func callEgoterra(_ sender: AnyObject) {
        UIApplication.shared.openURL(URL(string: "telprompt://0226502726")!)
    }
}
