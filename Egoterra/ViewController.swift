//
//  ViewController.swift
//  Egoterra
//
//  Created by Pablo coco on 2016. 3. 23..
//  Copyright © 2016년 Egoterra. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    @IBAction func goCoffee(_ sender: AnyObject) {
        if let uvc = self.storyboard?.instantiateViewController(withIdentifier: "CoffeeView"){
            
            //화면전환 에니메이션
            //uvc.modalTransitionStyle = UIModalTransitionStyle.CoverVertical
        
            //인자값으로 받은 뷰 컨트롤러로 화면 전환
            //self.presentViewController(uvc, animated: true, completion: nil)
            
            //네비게이션 컨트롤러를 통한 화면 전환
            self.navigationController?.pushViewController(uvc, animated: true)
        }
    }
    
    @IBAction func goTeaLatte(_ sender: AnyObject) {
        if let uvc = self.storyboard?.instantiateViewController(withIdentifier: "TeaLatteView"){
            self.navigationController?.pushViewController(uvc, animated: true)
        }
    }
    
    @IBAction func goJuiceAde(_ sender: AnyObject) {
        if let uvc = self.storyboard?.instantiateViewController(withIdentifier: "JuiceAdeView"){
            self.navigationController?.pushViewController(uvc, animated: true)
        }
    }
    
    @IBAction func goSmoothie(_ sender: AnyObject) {
        if let uvc = self.storyboard?.instantiateViewController(withIdentifier: "SmoothieView"){
            self.navigationController?.pushViewController(uvc, animated: true)
        }
    }
    
    @IBAction func goBakery(_ sender: AnyObject) {
        if let uvc = self.storyboard?.instantiateViewController(withIdentifier: "BakeryView"){
            self.navigationController?.pushViewController(uvc, animated: true)
        }
    }
    
    @IBAction func callEgoterra(_ sender: AnyObject){
//        var url:NSURL = NSURL(string: "tel://0226502726")!
//        UIApplication.sharedApplication().openURL(url)
        
        UIApplication.shared.openURL(URL(string: "telprompt://0226502726")!)
    }
    
}





















