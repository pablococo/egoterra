//
//  SmoothiesViewController.swift
//  Egoterra
//
//  Created by Pablo coco on 2016. 3. 25..
//  Copyright © 2016년 Egoterra. All rights reserved.
//

import UIKit

class SmoothiesViewController : UITableViewController{
    var SmoothiesList = Array<SmoothieVO>()
    
    override func viewDidLoad() {
        var ssvo = SmoothieVO()
        
        ssvo.thumbnail = "4-1.png"
        self.SmoothiesList.append(ssvo)
        
        ssvo = SmoothieVO()
        ssvo.thumbnail = "4-2.png"
        self.SmoothiesList.append(ssvo)
        
        ssvo = SmoothieVO()
        ssvo.thumbnail = "4-3.png"
        self.SmoothiesList.append(ssvo)
        
        ssvo = SmoothieVO()
        ssvo.thumbnail = "4-4.png"
        self.SmoothiesList.append(ssvo)
        
        ssvo = SmoothieVO()
        ssvo.thumbnail = "4-5.png"
        self.SmoothiesList.append(ssvo)
        
        ssvo = SmoothieVO()
        ssvo.thumbnail = "4-6.png"
        self.SmoothiesList.append(ssvo)
        
        ssvo = SmoothieVO()
        ssvo.thumbnail = "4-7.png"
        self.SmoothiesList.append(ssvo)
        
        ssvo = SmoothieVO()
        ssvo.thumbnail = "4-8.png"
        self.SmoothiesList.append(ssvo)
        
        ssvo = SmoothieVO()
        ssvo.thumbnail = "4-9.png"
        self.SmoothiesList.append(ssvo)
        
        ssvo = SmoothieVO()
        ssvo.thumbnail = "4-10.png"
        self.SmoothiesList.append(ssvo)
        
        ssvo = SmoothieVO()
        ssvo.thumbnail = "4-11.png"
        self.SmoothiesList.append(ssvo)
        
        ssvo = SmoothieVO()
        ssvo.thumbnail = "4-12.png"
        self.SmoothiesList.append(ssvo)
        
        ssvo = SmoothieVO()
        ssvo.thumbnail = "4-13.png"
        self.SmoothiesList.append(ssvo)
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.SmoothiesList.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let row = self.SmoothiesList[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ListCell")as! SmoothiesCell
        cell.thumbnail.image = UIImage(named: row.thumbnail!)
        return cell
    }
    @IBAction func callEgoterra(_ sender: AnyObject) {
        UIApplication.shared.openURL(URL(string: "telprompt://0226502726")!)
    }
}
