//
//  CoffeeViewController.swift
//  Egoterra
//
//  Created by Pablo coco on 2016. 3. 23..
//  Copyright © 2016년 Egoterra. All rights reserved.
//

import UIKit

class CoffeeViewController: UITableViewController{
    
    //테이블 뷰를 구성할 리스트 데이터를 담을 배열 변수
    var list = Array<CoffeeVO>()
    
    override func viewDidLoad() {
        var cvo = CoffeeVO()
        
        cvo.thumbnail = "1-1.png"
        
        self.list.append(cvo)
        
        cvo = CoffeeVO()
        cvo.thumbnail = "1-2.png"
        self.list.append(cvo)
        
        cvo = CoffeeVO()
        cvo.thumbnail = "1-3.png"
        self.list.append(cvo)
        
        cvo = CoffeeVO()
        cvo.thumbnail = "1-4.png"
        self.list.append(cvo)
        
        cvo = CoffeeVO()
        cvo.thumbnail = "1-5.png"
        self.list.append(cvo)
        
        cvo = CoffeeVO()
        cvo.thumbnail = "1-6.png"
        self.list.append(cvo)
        
        cvo = CoffeeVO()
        cvo.thumbnail = "1-7.png"
        self.list.append(cvo)
        
        cvo = CoffeeVO()
        cvo.thumbnail = "1-8.png"
        self.list.append(cvo)
        
        cvo = CoffeeVO()
        cvo.thumbnail = "1-9.png"
        self.list.append(cvo)
        
        cvo = CoffeeVO()
        cvo.thumbnail = "1-10.png"
        self.list.append(cvo)
        
        cvo = CoffeeVO()
        cvo.thumbnail = "1-11.png"
        self.list.append(cvo)
        
        cvo = CoffeeVO()
        cvo.thumbnail = "1-12.png"
        self.list.append(cvo)
        
        cvo = CoffeeVO()
        cvo.thumbnail = "1-13.png"
        self.list.append(cvo)
        
        cvo = CoffeeVO()
        cvo.thumbnail = "1-14.png"
        self.list.append(cvo)
        
        cvo = CoffeeVO()
        cvo.thumbnail = "1-15.png"
        self.list.append(cvo)
        
        cvo = CoffeeVO()
        cvo.thumbnail = "1-16.png"
        self.list.append(cvo)
        
        cvo = CoffeeVO()
        cvo.thumbnail = "1-17.png"
        self.list.append(cvo)
        
        cvo = CoffeeVO()
        cvo.thumbnail = "1-18.png"
        self.list.append(cvo)
        
        cvo = CoffeeVO()
        cvo.thumbnail = "1-19.png"
        self.list.append(cvo)
        
        cvo = CoffeeVO()
        cvo.thumbnail = "1-20.png"
        self.list.append(cvo)
        
        cvo = CoffeeVO()
        cvo.thumbnail = "1-21.png"
        self.list.append(cvo)
        
        cvo = CoffeeVO()
        cvo.thumbnail = "1-22.png"
        self.list.append(cvo)
        
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.list.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let row = self.list[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ListCell") as! CoffeeCell
        
        cell.thumbnail.image = UIImage(named: row.thumbnail!)

        return cell
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        NSLog("Touch Table Row at %d", indexPath.row)
    }
    
    @IBAction func callEgoterra(_ sender: AnyObject) {
        UIApplication.shared.openURL(URL(string: "telprompt://0226502726")!)
    }
}


