//
//  TeaLatteViewController.swift
//  Egoterra
//
//  Created by Pablo coco on 2016. 3. 24..
//  Copyright © 2016년 Egoterra. All rights reserved.
//

import UIKit

class TeaLatteViewController : UITableViewController {
    
    
    //테이블 뷰를 구성할 리스트 데이터를 담을 배열 변수
    var teaList = Array<TeaLatteVO>()
    
    override func viewDidLoad() {
        let tvo = TeaLatteVO()
        
        tvo.thumbnail = "2-1.png"
        
        self.teaList.append(tvo)
        
//        cvo = TeaLatteVO()
//        cvo.thumbnail = "2_2_green.png"
//        self.list.append(cvo)
//        
//        cvo = TeaLatteVO()
//        cvo.thumbnail = "2_3_black.png"
//        self.list.append(cvo)
//        
//        cvo = TeaLatteVO()
//        cvo.thumbnail = "2_4_black.png"
//        self.list.append(cvo)
//        
//        cvo = TeaLatteVO()
//        cvo.thumbnail = "2_5_choco.png"
//        self.list.append(cvo)
//        
//        cvo = TeaLatteVO()
//        cvo.thumbnail = "2_7_earl.png"
//        self.list.append(cvo)
//        
//        cvo = TeaLatteVO()
//        cvo.thumbnail = "2_8_earl.png"
//        self.list.append(cvo)
//        
//        cvo = TeaLatteVO()
//        cvo.thumbnail = "2_9_camo.png"
//        self.list.append(cvo)
//        
//        cvo = TeaLatteVO()
//        cvo.thumbnail = "2_10_camo.png"
//        self.list.append(cvo)
//        
//        cvo = TeaLatteVO()
//        cvo.thumbnail = "2_11_andong.png"
//        self.list.append(cvo)
//        
//        cvo = TeaLatteVO()
//        cvo.thumbnail = "2_12_andong.png"
//        self.list.append(cvo)
//        
//        cvo = TeaLatteVO()
//        cvo.thumbnail = "2_13_peach.png"
//        self.list.append(cvo)        
        
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.teaList.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let row = self.teaList[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ListCell") as! TeaLatteCell
        
        cell.thumbnail.image = UIImage(named: row.thumbnail!)
        
        return cell
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        NSLog("Touch Table Row at %d", indexPath.row)
    }
}
