//
//  JuiceAdeViewController.swift
//  Egoterra
//
//  Created by Pablo coco on 2016. 3. 25..
//  Copyright © 2016년 Egoterra. All rights reserved.
//

import UIKit

class JuiceAdeViewController: UITableViewController {
    
    var JuiceAdeList = Array<JuiceAdeVO>()
    
    override func viewDidLoad() {
        var jvo = JuiceAdeVO()
//        jvo.title = "Dark"
        jvo.thumbnail = "3-1.png"
        
        self.JuiceAdeList.append(jvo)
        
        jvo = JuiceAdeVO()
        jvo.thumbnail = "3-2.png"
        self.JuiceAdeList.append(jvo)
        
        jvo = JuiceAdeVO()
        jvo.thumbnail = "3-3.png"
        self.JuiceAdeList.append(jvo)
        
        jvo = JuiceAdeVO()
        jvo.thumbnail = "3-4.png"
        self.JuiceAdeList.append(jvo)
        
        jvo = JuiceAdeVO()
        jvo.thumbnail = "3-5.png"
        self.JuiceAdeList.append(jvo)
        
        jvo = JuiceAdeVO()
        jvo.thumbnail = "3-6.png"
        self.JuiceAdeList.append(jvo)
        
        jvo = JuiceAdeVO()
        jvo.thumbnail = "3-7.png"
        self.JuiceAdeList.append(jvo)
        
        jvo = JuiceAdeVO()
        jvo.thumbnail = "3-8.png"
        self.JuiceAdeList.append(jvo)
        
        jvo = JuiceAdeVO()
        jvo.thumbnail = "3-9.png"
        self.JuiceAdeList.append(jvo)
        
        jvo = JuiceAdeVO()
        jvo.thumbnail = "3-10.png"
        self.JuiceAdeList.append(jvo)
        
        jvo = JuiceAdeVO()
        jvo.thumbnail = "3-11.png"
        self.JuiceAdeList.append(jvo)
        
        jvo = JuiceAdeVO()
        jvo.thumbnail = "3-12.png"
        self.JuiceAdeList.append(jvo)
        
        jvo = JuiceAdeVO()
        jvo.thumbnail = "3-13.png"
        self.JuiceAdeList.append(jvo)
        
        jvo = JuiceAdeVO()
        jvo.thumbnail = "3-14.png"
        self.JuiceAdeList.append(jvo)
        
        jvo = JuiceAdeVO()
        jvo.thumbnail = "3-15.png"
        self.JuiceAdeList.append(jvo)
        
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.JuiceAdeList.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let row = self.JuiceAdeList[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ListCell") as! JuiceAdeCell
//        cell.textLabel?.text = row.title
        
        cell.thumbnail.image = UIImage(named: row.thumbnail!)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        NSLog("Touch Table Row at %d", indexPath.row)
    }
    @IBAction func callEgoterra(_ sender: AnyObject) {
        UIApplication.shared.openURL(URL(string: "telprompt://0226502726")!)
    }

}
