//
//  BakeryViewController.swift
//  Egoterra
//
//  Created by Pablo coco on 2016. 3. 25..
//  Copyright © 2016년 Egoterra. All rights reserved.
//

import UIKit

class BakeryViewController : UITableViewController{
    var BakeryList = Array<BakeryVO>()
    
    override func viewDidLoad() {
        var bvo = BakeryVO()
        bvo.thumbnail = "5-1.png"
        self.BakeryList.append(bvo)
        
        bvo = BakeryVO()
        bvo.thumbnail = "5-2.png"
        self.BakeryList.append(bvo)
        
        bvo = BakeryVO()
        bvo.thumbnail = "5-3.png"
        self.BakeryList.append(bvo)
        
        bvo = BakeryVO()
        bvo.thumbnail = "5-4.png"
        self.BakeryList.append(bvo)
        
        bvo = BakeryVO()
        bvo.thumbnail = "5-5.png"
        self.BakeryList.append(bvo)
        
        bvo = BakeryVO()
        bvo.thumbnail = "5-6.png"
        self.BakeryList.append(bvo)
        
        bvo = BakeryVO()
        bvo.thumbnail = "5-7.png"
        self.BakeryList.append(bvo)
        
        bvo = BakeryVO()
        bvo.thumbnail = "5-8.png"
        self.BakeryList.append(bvo)
        
        bvo = BakeryVO()
        bvo.thumbnail = "5-9.png"
        self.BakeryList.append(bvo)
        
        bvo = BakeryVO()
        bvo.thumbnail = "5-10.png"
        self.BakeryList.append(bvo)
        
        bvo = BakeryVO()
        bvo.thumbnail = "5-11.png"
        self.BakeryList.append(bvo)
        
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.BakeryList.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let row = self.BakeryList[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ListCell") as! BakeryCell
        cell.textLabel?.text = row.title
        
        cell.thumbnail.image = UIImage(named: row.thumbnail!)
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        NSLog("Touch Table Row at %d", indexPath.row)
    }
    @IBAction func callEgoterra(_ sender: AnyObject) {
        UIApplication.shared.openURL(URL(string: "telprompt://0226502726")!)
    }
}
